import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import PrimeVue from 'primevue/config';
import Password from 'primevue/password';
import axios from 'axios'
import VueAxios from 'vue-axios'
import Toast from 'primevue/toast';
import ToastService from 'primevue/toastservice';
import Dialog from 'primevue/dialog';
import Button from 'primevue/button';
import Card from 'primevue/card';
import Column from 'primevue/column';
import Checkbox from 'primevue/checkbox';
import DataTable from 'primevue/datatable';
import Dropdown from 'primevue/dropdown';
import Listbox from 'primevue/listbox';
import InputText from 'primevue/inputtext';
import Toolbar from 'primevue/toolbar';
import TabView from 'primevue/tabview';
import TabPanel from 'primevue/tabpanel';
import '@/assets/styles.scss';
const app = createApp(App)
app.use(router);
app.use(PrimeVue, { ripple: true });
app.use(VueAxios, axios);
app.use(ToastService);
app.component('Button', Button);
app.component('Card', Card);
app.component('Column', Column);
app.component('Checkbox', Checkbox);
app.component('DataTable', DataTable);
app.component('Dropdown', Dropdown);
app.component('Dialog', Dialog);
app.component('InputText', InputText);
app.component('Listbox', Listbox);
app.component('Password', Password);
app.component('Toast', Toast);
app.component('Toolbar',Toolbar);
app.component('TabView', TabView);
app.component('TabPanel',TabPanel);
app.mount('#app')
