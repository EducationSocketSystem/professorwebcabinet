import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'
import AppLayout from '@/layout/AppLayout.vue';
import LandingPage from '../components/LandingPage.vue';
import Login from '../views/pages/Login.vue'
const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    
    {
          path: '',
          name: 'landing',
          component:LandingPage
    },
    {
      path: '/login',
      name: 'login',
      component:Login
    },

    {
      path: '/',
      component: AppLayout,
      children: [
       
        {
          path: '/home',
          component: HomeView
        },
        {
          path: '/professorTable',
          name: 'professorTable',
          component: () => import('@/views/ProfessorTable.vue')
        },
        {
          path: '/register',
          name: 'register',
          component: () => import('@/views/register.vue')
        }

      ]
    }
  ]
})

export default router
